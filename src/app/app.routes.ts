import { Routes } from '@angular/router';
import { NavMenuComponent } from './NavBar/nav-menu/nav-menu.component';
import { HomeComponent } from './Dashboard/home/home.component';
import { HomePageComponent } from './Dashboard/home-page/home-page.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },

    {
        path: 'navmenu',
        component: NavMenuComponent
    },
    {
        path: 'home',
        component: HomePageComponent
    },
];
