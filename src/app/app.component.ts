import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { NavMenuComponent } from './NavBar/nav-menu/nav-menu.component';
import { HomeComponent } from './Dashboard/home/home.component';
import { FooterComponent } from './NavBar/footer/footer.component';
import { OwlCarousel } from 'ngx-owl-carousel';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, NavMenuComponent, HomeComponent, FooterComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'Rox';
}
